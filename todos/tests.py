from django.test import TestCase
from .models import Todo


class TodoModelTest(TestCase):

    @classmethod
    def setUpTestData(cls):
        Todo.objects.create(title='first todo', body='test body')

    def test_title_content(self):
        todo = Todo.objects.get(id=1)
        expected = 'first todo'
        actual = f"{todo.title}"
        self.assertEquals(actual,expected)

    def test_body_content(self):
        todo = Todo.objects.get(id=1)
        expected = 'test body'
        actual = f"{todo.body}"
        self.assertEquals(actual,expected)